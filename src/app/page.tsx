"use client";

import Heart from "@/components/heart";
import Image from "next/image";
import { useState } from "react";
import { Playfair_Display, Dancing_Script } from "next/font/google";

const plainText = Playfair_Display({subsets: ["latin"]})
const specialText = Dancing_Script({subsets: ["latin"]})

export default function Home() {
  const [letterOpen, setLetterOpen] = useState(false)
  const [cardOpen, setCardOpen] = useState(false)
  const [yesOrNo, setYesOrNo] = useState<boolean | undefined>(undefined)
  return (
    <main className="flex min-h-screen flex-col items-center justify-center">
      <div className="max-w-full w-[40rem] m-16">
        <div className="relative w-[40rem] h-96">
          <div className="inset-0 bg-blue-50 border-4 border-blue-100 absolute drop-shadow-md">
            <div className="inset-x-0 bottom-0 h-60 bg-blue-50 border-b-4 border-blue-100 absolute drop-shadow-md -scale-y-100 z-10"></div>
            <div className={"absolute inset-4 " + (letterOpen ? "card-popout" : "")}>
              <div className="absolute inset-0 bg-white drop-shadow-md border-neutral-100 border-t-4 flex justify-evenly items-center">
                  <button className={plainText.className + " text-neutral-400 tracking-wider text-3xl border-4 border-neutral-200 rounded-xl px-3 pb-2 hover:bg-neutral-50"} onClick={() => {setYesOrNo(true)}}>yes</button>
                  <div className="w-40 flex items-center justify-center">{yesOrNo != undefined && (<>
                    {yesOrNo ? (
                      <div className="relative w-24 h-24 fade-in-anim">
                        <Heart className="absolute inset-0 text-red-500 w-24 h-24 " />
                        <Heart className="absolute inset-0 text-red-500 w-24 h-24 heart-pulse-straight-anim" />
                      </div>
                    ) : (
                      <p className={plainText.className + " fade-in-anim text-7xl"}>:(</p>
                    )}
                  </>)}</div>
                  <button className={plainText.className + " text-neutral-400 tracking-wider text-3xl border-4 border-neutral-200 rounded-xl px-3 pb-2 hover:bg-neutral-50"} onClick={() => {setYesOrNo(false)}}>no</button>
              </div>
              <div className={"absolute inset-0 bottom-2 bg-white drop-shadow-md origin-top transition-all duration-250 " + (letterOpen ? "" : "pointer-events-none ") + (cardOpen ? "card-open-anim" : "slightly-open cursor-pointer")} onClick={() => {setCardOpen(true)}}>
                <div className={"absolute inset-2 flex flex-col items-center justify-center gap-12 duration-[0s] delay-[375ms] transition-all " + (cardOpen ? "opacity-0" : "")}>
                  <p className={plainText.className + " text-neutral-400 tracking-wider text-3xl"}>will you be my</p>
                  <h1 className={specialText.className + " text-red-500 text-9xl"}>valentine</h1>
                  <Heart className="absolute w-7 h-7 right-[11.2rem] bottom-[7.7rem] rotate-[25deg] text-red-500 heart-pulse-anim"/>
                  <Heart className="absolute w-7 h-7 right-[11.2rem] bottom-[7.7rem] rotate-[25deg] text-red-500"/>
                </div>
              </div>
            </div>
          </div>
          <div className={"absolute inset-x-0 top-0 h-48 bg-blue-50 border-blue-100 border-4 rounded-b-3xl hover:drop-shadow-md cursor-pointer transition-shadow origin-top transition-all duration-500 " + (letterOpen ? "envelope-flap-anim drop-shadow-md" : "")} onClick={() => {setLetterOpen(true)}}>
            <Heart className="absolute -bottom-10 inset-x-0 m-auto w-16 h-16 text-red-500"/>
          </div>
        </div>
      </div>
    </main>
  );
}
